import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class LoginService {

  constructor(private http:HttpClient) {

   }

   public login(user,pwd)
   {
     return(this.http.post('http://localhost:8080/login',[{user:user,pwd:pwd}],{withCredentials:true}))
     
   }
     public isLoggined()
   {
     this.http.post('http://localhost:8080/logined',[{user:"test"}]).subscribe(data=>{
       if(data['success']==true)
         return true
     })
     
   }

}

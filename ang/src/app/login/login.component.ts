import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import{LoginService} from '../login.service'
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  private pwd
  private usr
 private loginData=''
  constructor(private router:Router,private loginSuc:LoginService) {
    this.pwd =''
    this.usr=''

  }

  ngOnInit() {
  }
  login(usr,pwd) {
    this.loginSuc.login(usr,pwd).subscribe(data=>{
         if(data['success']==true)
        this.router.navigate(['./dashboard'])
    })
      // if(this.loginData['username']==this.usr && this.loginData['password']==this.pwd)
      //   {
       
        // }
      
  }
}

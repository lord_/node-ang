import { Injectable } from '@angular/core';
import{ LoginService } from './login.service';
import { Router, CanActivate } from '@angular/router';

@Injectable()
export class AuthGaurdService implements CanActivate {

  constructor(public auth: LoginService, public router: Router) { }

  canActivate(): boolean {
    if (!this.auth.isLoggined()) {
      // this.router.navigate(['login']);
      return true;
    }
    return true;
  }

}

import { LoginFormPage } from './app.po';

describe('login-form App', () => {
  let page: LoginFormPage;

  beforeEach(() => {
    page = new LoginFormPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
